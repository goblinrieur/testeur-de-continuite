# Continuity tester

Read [LICENSE](./LICENSE), for information about it, this is quite permissive one.

This is a quite cheap & simple continuity tester, with minimal security.

It has :

- A protection fuse

- A protection TVS

- A push button to trigger Power-ON for a short time the tester.

- Choosing transistors & MOSFETs by equivalences : [all transistors site](https://alltransistors.com/)

# Components & notes

- fuse F1 can be shorted by a bridge, if you don't have one.

- Q1 might be a [IRFD9020](./IRFD9020.pdf), as many parts in the project, it can be replaced by its equivalent.

- R14/R15 divider can be recalculated with other resistor values.

- D11 can be a 5.6v zener diode or at least a 5.1v one.

- R2 - D2 power on led indicator can be unused, & the color of led can be changed _(adapting R2)_.

- BZ1 must be a piezzo buzzer without internal oscillator _(you can make surgery to bread oscillator if you have only one with its oscillator)_. Only space between pads is really important if you have a differently packaged one.

- Use only 1/4 watts resistors 5% tolerance _(to keep it cheap)_.

- Use 25v & more capacitors _(in 0604, it should be 50v as default, even for high values it can be 16v)_.

# pdf's

[schema](./schema.pdf) & [in colors](./schema_color.pdf)

[PCB](./PCB.pdf)

Q1 data-sheet [IRFD9020](./IRFD9020.pdf)

Q2 data-sheet [2SK28551](./2sK2851.pdf)

LM393 dual differential comparator data-sheet [lm393](./lm393-d.pdf)

Micro power Phase-Locked Loop data-sheet [cd4046](./CD4046.pdf)

CMOS quad 2-input NAND schmitt trigger data-sheet [cd4093](./cd4093b_TI.pdf)
